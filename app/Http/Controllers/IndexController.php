<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class IndexController extends Controller
{
	public function fetchSelectPostcodes(){
		$results = 	DB::table('postcodes_geo')
					->get();

		return $results;
	}

    public function minMaxDistance(Request $request){

    	$lat = $request->lat;
    	$lng = $request->lng;
    	$distance = $request->radius;

		// for km
		$radius = 6371;

		$maxlat = $lat + rad2deg($distance / $radius);
		$minlat = $lat - rad2deg($distance / $radius);

		$maxlng = $lng + rad2deg($distance / $radius / cos(deg2rad($lat)));
		$minlng = $lng - rad2deg($distance / $radius / cos(deg2rad($lat)));

		$results = 	DB::table('postcodes_geo')
					->whereBetween('latitude', [$minlat, $maxlat])
					->whereBetween('longitude', [$minlng, $maxlng])
					->get();

		foreach ($results as $i => $result) {
		    $resultDistance = $this->_distance($lat, $lng, $result->latitude, $result->longitude);
		    if ($resultDistance > $distance) {
		        unset($results[$i]);
		    }
		}

		return $results;
    }

	private function _distance($lat1, $lng1, $lat2, $lng2) {
	    $lat1 = deg2rad($lat1);
	    $lng1 = deg2rad($lng1);
	    $lat2 = deg2rad($lat2);
	    $lng2 = deg2rad($lng2);

	    $distance = acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($lng1 - $lng2));

	    return 6371 * $distance;
	}
}
