# README australian-postcode #

This is technical test.

## Installation 

Clone the repo
```
git clone https://aalviian@bitbucket.org/aalviian/australian-postcode.git
```

Move to the newly created folder and install all dependencies:
```
cd australian-postcode
composer install
npm install
```

Finally, generate the application key 
```
php artisan key:generate
```

Open your favorite browser and visit the newly created app.
```
php artisan serve (first terminal/command prompt)
npm run watch (second terminal/command prompt)
```


## Screenshot
![ss1.PNG](https://bytebucket.org/aalviian/lemonilo/raw/1ace03e10eabdaceede685f14189e0100bda428f/public/ss1.PNG)
![ss2.PNG](https://bytebucket.org/aalviian/lemonilo/raw/1ace03e10eabdaceede685f14189e0100bda428f/public/ss2.PNG)